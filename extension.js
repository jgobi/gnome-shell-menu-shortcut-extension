const { Meta, St  } = imports.gi;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const WindowMenu = imports.ui.windowMenu;
const Clutter = imports.gi.Clutter;

const shortcutMap = new Map([
    [_("Maximize"), "x"],
    [_("Unmaximize"), "x"],
    [_("Minimize"), "n"],
]);

let originalMenuInit, originalBuildMenu;

function init() {
}

function enable() {
    originalMenuInit = PopupMenu.PopupMenu.prototype["_init"];
    PopupMenu.PopupMenu.prototype["_init"] = function() {
        originalMenuInit.apply(this, arguments);
        this.actor.connect('key-press-event', (actor, event) => {
            let r = this._onKeyPress.apply(this, [actor, event]);
            if (r == Clutter.EVENT_STOP) {
                return r;
            }
            let items = this._getMenuItems();
            for (let item of items) {
                if (item.shortcut !== undefined && item.shortcut.charCodeAt(0) == event.get_key_symbol()) {
                    log("SHORTCUT " + item.shortcut + " TRIGGERED!");
                    item.activate(event);
                    return Clutter.EVENT_STOP;
                }
            }
            return Clutter.EVENT_PROPAGATE;
        });
    }

    originalBuildMenu = WindowMenu.WindowMenu.prototype['_buildMenu'];
    WindowMenu.WindowMenu.prototype['_buildMenu'] = function(window) {
        originalBuildMenu.apply(this, arguments);
        let items = this._getMenuItems();
        for (let pos in items) {
            let item = items[pos];

            let text = item.label.text;
            if (shortcutMap.has(text)) {
                let itemWithShortcut = new PopupMenuItemWithShortcut(text, shortcutMap.get(text))
                itemWithShortcut.connect('activate', (m, e) => {
                    item.emit('activate');
                });
                itemWithShortcut.connect('destroy', (m, e) => {
                    item.emit('destory');
                });
                this.addMenuItem(itemWithShortcut, pos);
                this.box.remove_child(item.actor);
            }
        }
    }
}

function disable() {
    PopupMenu.PopupMenu.prototype["_init"] = originalMenuInit;
    WindowMenu.WindowMenu.prototype['_buildMenu'] = originalBuildMenu;
}

const PopupMenuItemWithShortcut = class extends PopupMenu.PopupBaseMenuItem {
    constructor(text, shortcut, params) {
        super(params);

        let label = new St.BoxLayout();
        let i = text.indexOf(shortcut);
        if (i >= 0) {
            label.add_child(new St.Label({ text: text.substring(0, i) }));
            label.add_child(new St.Label({ text: shortcut, style_class: "menu-shortcut" }));
            label.add_child(new St.Label({ text: text.substring(i + 1) }));
        } else {
            label.add_child(new St.Label({ text: text + " " }));
            label.add_child(new St.Label({ text: "(" + shortcut + ")", style_class: "menu-shortcut" }));
        }

        this.shortcut = shortcut;
        this.label = label;
        this.actor.add_child(this.label);
        this.actor.label_actor = this.label
    }

};

function getMethods(obj) {
  var result = [];
  for (var id in obj) {
    try {
      if (typeof(obj[id]) == "function") {
        result.push(id + ": " + obj[id].toString());
      }
    } catch (err) {
      result.push(id + ": inaccessible");
    }
  }
  return result;
}
